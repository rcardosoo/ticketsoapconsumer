package br.com.novio.auth;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

import br.com.novio.auth.client.TicketClient;
import br.com.novio.auth.config.SoapClientConfig;
import br.com.novio.auth.wsdl.TicketRequest;
import br.com.novio.auth.wsdl.TicketRequest.Dados.Entry;
import br.com.novio.auth.wsdl.TicketResponse;

@SpringBootApplication
public class TicketSoapConsumerApplication implements CommandLineRunner {

	@Autowired
	private Environment env;

	public static void main(String[] args) {
		SpringApplication.run(TicketSoapConsumerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SoapClientConfig.class);
		TicketClient client = context.getBean(TicketClient.class);
		TicketRequest request = new TicketRequest();
		request.setSistema(env.getProperty("sistema"));
		request.setExpiracao(Integer.parseInt(env.getProperty("expiracao")));

		Properties prop = new Properties();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();           
		InputStream stream = loader.getResourceAsStream("application.properties");
		prop.load(stream);

		TicketRequest.Dados dados = new TicketRequest.Dados();
		List<TicketRequest.Dados.Entry> entries = new ArrayList<>();	
		
		Set<String> keys = prop.stringPropertyNames();
		for (String key : keys) {
			if (!prop.getProperty(key).isEmpty()) {
				entries.add(new Entry(key, prop.getProperty(key)));
			}
		}
		
		request.setDados(dados);

		try {
			TicketResponse response = client.autenticar(request);
			System.out.println("Consulta realizada | ticket: " + response.getTicket());	
		} catch(Exception e) {
			System.out.println("Não funcionou - " + e.getMessage());
		}
	}
}