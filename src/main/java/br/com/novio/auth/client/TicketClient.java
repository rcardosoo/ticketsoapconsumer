package br.com.novio.auth.client;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import br.com.novio.auth.wsdl.TicketRequest;
import br.com.novio.auth.wsdl.TicketResponse;

public class TicketClient extends WebServiceGatewaySupport {
	
	public TicketResponse autenticar(TicketRequest ticketRequest) {
		Object retorno = (Object) getWebServiceTemplate().marshalSendAndReceive(ticketRequest);
		return (TicketResponse) retorno;
	}
}
