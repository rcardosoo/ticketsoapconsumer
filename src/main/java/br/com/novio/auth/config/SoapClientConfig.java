package br.com.novio.auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import br.com.novio.auth.client.TicketClient;


@Configuration
public class SoapClientConfig {
	
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("br.com.novio.auth.wsdl");
		return marshaller;
	}

	@Bean
	public TicketClient tituloCliente(Jaxb2Marshaller marshaller) {
		TicketClient client = new TicketClient();
		client.setDefaultUri("https://ymbdlb.santander.com.br/dl-ticket-services/TicketEndpointService");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
}

